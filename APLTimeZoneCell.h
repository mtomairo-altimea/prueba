
/*

 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */

@import UIKit;

@interface APLTimeZoneCell : UITableViewCell

@property (nonatomic, weak, readonly) UILabel *nameLabel;
@property (nonatomic, weak, readonly) UILabel *timeLabel;
@property (nonatomic, weak, readonly) UIImageView *timeImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblDescripcion;

@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;

@property (weak, nonatomic) IBOutlet UILabel *lblLugar;
@property (weak, nonatomic) IBOutlet UILabel *lblHoraFecha;
@end
