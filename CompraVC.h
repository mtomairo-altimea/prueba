//
//  CompraVC.h
//  JoinUs-iPhone
//
//  Created by bruno on 27/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface CompraVC : UIViewController<UIGestureRecognizerDelegate,MKMapViewDelegate,CLLocationManagerDelegate>
{
    NSMutableDictionary *dicEventoRecieved;
    CLLocationManager *locationManager;
    NSString *lat;
    NSString *lon;
}
- (IBAction)onclickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgvEvento;
@property (weak, nonatomic) IBOutlet MKMapView *mapKitView;
@property (weak, nonatomic) IBOutlet UIView *viewBoton;
@property (weak, nonatomic) IBOutlet UIView *viewOscuro;
@property (weak, nonatomic) IBOutlet UIView *viewArriba;
@property (weak, nonatomic) IBOutlet UIView *viewAbajo;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewArriba;
- (IBAction)onClicComprar:(id)sender;
- (IBAction)onClickComoLlegar:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;
@property (weak, nonatomic) IBOutlet UILabel *lblDireccion;
@property (weak, nonatomic) IBOutlet UILabel *lblDistrito;
@property (weak, nonatomic) IBOutlet UIButton *btnCerrar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLlegar;
@property (weak, nonatomic) IBOutlet UIImageView *imgvHeaderMapa;
- (IBAction)onClickCerrar:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *onClickShare;
@property (weak, nonatomic) IBOutlet UITextView *txtvDescripcion;
- (IBAction)onCLickShare:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblHora;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaHoraFin;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblfecha;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;

@property (nonatomic,strong) UIImage * image;

@end
