//
//  CompraVC.m
//  JoinUs-iPhone
//
//  Created by bruno on 27/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import "CompraVC.h"
#import <FacebookSDK/FacebookSDK.h>

@interface CompraVC ()

@end

@implementation CompraVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mapKitView.delegate = self;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    //Autorizacion gps
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    

    
    //Recognizer
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate=self;
    
    [self.viewBoton addGestureRecognizer:tapGestureRecognizer];
    
    _btnCerrar.alpha = 0;
    _btnLlegar.alpha = 0;
    
    //Obtener datos de UserDefaults
    dicEventoRecieved = [[NSUserDefaults standardUserDefaults] objectForKey:@"dictSend"];
    
    //UIImage *imgEvento= [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dicEventoRecieved objectForKey:@"imgUrl"]]]];
    
    
   // UIImage *imgEvento= [UIImage imageWithData:[dicEventoRecieved objectForKey:@"dataImg"]];
    _imgvEvento.image = _image;
    _lblTitle.text = [dicEventoRecieved objectForKey:@"title"];
    _txtvDescripcion.text = [dicEventoRecieved objectForKey:@"description"];
    _lblPrecio.text = [NSString stringWithFormat:@"S./%@.00", [dicEventoRecieved objectForKey:@"price"]];
    _lblTitleHeader.text = [dicEventoRecieved objectForKey:@"title"];
    _lblfecha.text = [dicEventoRecieved objectForKey:@"fechaEnviar"];
    _lblMonth.text = [dicEventoRecieved objectForKey:@"MesEnviar"];
    _lblFechaHoraFin.text = [dicEventoRecieved objectForKey:@"dateEnd"];
    _lblHora.text = [dicEventoRecieved objectForKey:@"horaEnviar"];
    _lblDistrito.text = [dicEventoRecieved objectForKey:@"city"];
    _lblDireccion.text = [dicEventoRecieved objectForKey:@"address"];
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    

    
    [UIView animateWithDuration:0.05
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         [self.scrollViewArriba setFrame:CGRectMake(0, 0, 320, 410)];
                         [self.btnLlegar setFrame:CGRectMake(75, 540, 80, 50)];
                         
                         [self.viewBoton setHidden:YES];
                                                 
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         
                         [UIView animateWithDuration:0.3
                                               delay:0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              
                                              [self.scrollViewArriba setFrame:CGRectMake(0, 0, 320, 0.5)];
                                              [self.viewAbajo setFrame:CGRectMake(0, 567, 320, 0.5)];
                                              
                                              self.viewOscuro.alpha = 0;
                                              [self.btnLlegar setFrame:CGRectMake(64, 530, 192, 30)];
                                              self.btnCerrar.alpha = 1;
                                              self.imgvHeaderMapa.alpha = 1;
                                              
                                              
                                              _btnLlegar.transform = CGAffineTransformMakeScale(1.5,1.5);
                                              _btnLlegar.alpha = 0.0f;
                                              
                                              [UIView beginAnimations:@"button" context:nil];
                                              [UIView setAnimationDuration:1];
                                              _btnLlegar.transform = CGAffineTransformMakeScale(1,1);
                                              _btnLlegar.alpha = 1.0f;
                                              
                                              [UIView commitAnimations];

                                              
                                              
                                          }
                                          completion:^(BOOL finished){
                                              

                                              
                                              [UIView animateWithDuration:0.3
                                                                    delay:0
                                                                  options:UIViewAnimationOptionCurveLinear
                                                               animations:^{
                                                                   
                                                                   MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
                                                                   NSString *lat= [dicEventoRecieved objectForKey:@"addresslat"];
                                                                   NSString *lon = [dicEventoRecieved objectForKey:@"addresslng"];
                                                                   
                                                                   region.center.latitude = [lat floatValue];
                                                                   region.center.longitude = [lon floatValue];
                                                                   region.span.longitudeDelta = 0.005f;
                                                                   region.span.longitudeDelta = 0.005f;
                                                                   [_mapKitView setRegion:region animated:YES];
                                                                   
                                                                   
                                                                   
                                                               }
                                                               completion:^(BOOL finished){
  
                                                                   
                                                               }];
                                              

                                              
                                          }];

                         

                     }];
    
    
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         CLLocationCoordinate2D coordinate;
                         lat= [dicEventoRecieved objectForKey:@"addresslat"];
                         lon = [dicEventoRecieved objectForKey:@"addresslng"];
                         
                         coordinate.latitude = [lat floatValue];
                         coordinate.longitude = [lon floatValue];
                         
                         MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                       
                         point.coordinate = coordinate;
                         [_mapKitView addAnnotation:point];
                         
                         
                         
                     }
                     completion:^(BOOL finished){
                         
      
                     }];


}




- (IBAction)onclickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClicComprar:(id)sender {
    
    NSLog(@"COmprar");
    
}

- (IBAction)onClickComoLlegar:(id)sender {
    NSLog(@"Llegar");
    
    [self navigateToLatitude:[lat doubleValue] longitude:[lon doubleValue]];
    
    
}
- (IBAction)onClickCerrar:(id)sender {
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [self.scrollViewArriba setFrame:CGRectMake(0, 0, 320, 403)];
                         [self.viewAbajo setFrame:CGRectMake(0, 468, 320, 100)];
                         [self.viewBoton setHidden:YES];
                         self.viewOscuro.alpha = 0.2;
                         
                         self.btnCerrar.alpha = 0;
                         self.imgvHeaderMapa.alpha = 0;

                         _btnLlegar.alpha = 0;
  
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
                         lat= [dicEventoRecieved objectForKey:@"addresslat"];
                         lon = [dicEventoRecieved objectForKey:@"addresslng"];
                         
                         region.center.latitude = [lat floatValue];
                         region.center.longitude = [lon floatValue];
                         region.span.longitudeDelta = 0.12f;
                         region.span.longitudeDelta = 0.12f;
                         [_mapKitView setRegion:region animated:YES];
                        
                         [self.viewBoton setHidden:NO];
                         _mapKitView.alpha=1;
                     }];

    

    

}

#pragma mark Mapkit delagates

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    static NSString *identifier = @"myAnnotation";
    MKAnnotationView * annotationView = (MKAnnotationView *)[_mapKitView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!annotationView)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.image = [UIImage imageNamed:@"mapa-anotationIcon.png"];
    }else {
        annotationView.annotation = annotation;
    }
    annotationView.tag = 1;
    return annotationView;
}

#pragma mark metodos utiles

- (void) navigateToLatitude:(double)latitude
                  longitude:(double)longitude
{
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:@"waze://"]]) {
        
        // Waze is installed. Launch Waze and start navigation
        NSString *urlStr =
        [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",
         latitude, longitude];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        
    } else {
        
        // Waze is not installed. Launch AppStore to install Waze app
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
}


- (IBAction)onCLickShare:(id)sender {
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"http://www.joinnus.com/act/the-voiders-presentacin-en-vivo-no-more-restrictions-ep/1720"];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                              //llamamos a whatsapp ahora
                                              NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send?text=Te%2C%20Recomiendo%2C%20este%2C%20Evento%2C%20http://www.joinnus.com/act/the-voiders-presentacin-en-vivo-no-more-restrictions-ep/1720!"];//use this method stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding to convert it with escape char
                                              if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                                                  [[UIApplication sharedApplication] openURL: whatsappURL];
                                              }
                                          }
                                      }];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"https://itunes.apple.com/mx/app/facebook/id284882215"]];
    }
    
    

   
}
@end
