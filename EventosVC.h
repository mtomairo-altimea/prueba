//
//  EventosVC.h
//  JoinUs-iPhone
//
//  Created by bruno on 22/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventosVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIButton* btnLike;
    NSMutableArray *arrEventos;
    NSMutableDictionary *dictResponse;
    NSMutableDictionary *dictSend;
    NSString *dateStartToSend;
    NSMutableArray *arrImgs;
    
    UIImage *imgAux;
    
    NSData *dataAux;
}

- (IBAction)onClickBuscarEvento:(UIButton *)sender;


@end
