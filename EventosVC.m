//
//  EventosVC.m
//  JoinUs-iPhone
//
//  Created by bruno on 22/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import "EventosVC.h"
#import "UIViewController+AMSlideMenu.h"
#import "UIColor+CreateMethods.h"
#import "SDSegmentedControl.h"
#import "FSParallaxTableViewCell.h"
#import "AFNetworking.h"
#import "SDWebImageManager.h"
#import "SVProgressHUD.h"
#import "CompraVC.h"
#import "ViewController.h"
#import "MainVC.h"
#import "APLTimeZoneCell.h"
#import "PruebaVC.h"


@interface EventosVC ()
@property (weak, nonatomic) IBOutlet SDSegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentHeight;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *imageNames;

@end

@implementation EventosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(muestraVCInicio) name:@"muestraVCInicio" object:nil];
    
    [self addLeftMenuButton];
    
    [self llamadaServicio];
    //custom SDsegment
    arrImgs = [[NSMutableArray alloc] init];
    ((SDSegmentedControl *)_segmentControl).arrowHeightFactor *= -1.0;

    SDSegmentedControl* sdSegmentedControl = (SDSegmentedControl *)_segmentControl;
    sdSegmentedControl.arrowPosition = sdSegmentedControl.arrowPosition == SDSegmentedArrowPositionBottom ? SDSegmentedArrowPositionTop : SDSegmentedArrowPositionBottom;
    

    self.segmentHeight.constant = 40;
    
    [_segmentControl setImage:[UIImage imageNamed:@"eventos-segmentSeparator"] forSegmentAtIndex:1];
    [_segmentControl setImage:[UIImage imageNamed:@"eventos-segmentSeparator"] forSegmentAtIndex:2];
    
    self.imageNames = @[@"eventos-eventoImagen1",
                        @"eventos-Imagen2",
                        @"img03.jpg",
                        @"img04.jpg",
                        @"img05.jpg",
                        @"img06.jpg",
                        @"img07.jpg",
                        @"img08.jpg",
                        @"img09.jpg",
                        @"img10.jpg",
                        @"img11.jpg"];
    
    [SVProgressHUD show];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDatasourceDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrEventos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TimeZoneCell";
    
    APLTimeZoneCell *cell = (APLTimeZoneCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // configureCell:cell forIndexPath: sets the text and image for the cell -- the method is factored out because it's also called during minuted-based updates.
    
    if (indexPath.row == 2) {
        //Scroll de prueba
        

        [self configureCell:cell forIndexPath:indexPath];
        
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 195, 320, 156)];
        scrollView.delegate =self;
        scrollView.tag =21;
        
        scrollView.contentSize = CGSizeMake(182*3, 156);
        
        UIImage *img1 =[UIImage imageNamed:@"recomendado1"];
        UIImage *img2 =[UIImage imageNamed:@"recomendado2"];
        UIImage *img3 =[UIImage imageNamed:@"recomendado3"];
        
        UIImageView *imgV1 = [[UIImageView alloc] initWithImage:img1];
        UIImageView *imgV2 = [[UIImageView alloc] initWithImage:img2];
        UIImageView *imgV3 = [[UIImageView alloc] initWithImage:img3];
        
        imgV1.frame = CGRectMake(0, 0, 182, 156);
        imgV2.frame = CGRectMake(182, 0, 182, 156);
        imgV3.frame = CGRectMake(364, 0, 182, 156);
        
        [scrollView addSubview:imgV1];
        [scrollView addSubview:imgV2];
        [scrollView addSubview:imgV3];
        
        [cell addSubview:scrollView];
        
        return cell;
        
    }

    
    
    
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    //Formateos Fecha ENd
    //Formateos fecha y hora.
    NSString *dateEnd= [[arrEventos objectAtIndex:indexPath.row] objectForKey:@"dateEnd"];
    
    NSDateFormatter *dfEnd = [[NSDateFormatter alloc] init];
    [dfEnd setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //[df setDateStyle:NSDateFormatterMediumStyle];
    NSDate *fechaFromStringEnd = [dfEnd dateFromString: dateEnd];
    
    NSDateComponents *componentsEnd = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitHour fromDate:fechaFromStringEnd];
    
    NSInteger dayEnd= [componentsEnd day];
    NSInteger hourEnd =[componentsEnd hour];
    
    
    
    NSDateFormatter *formatterEnd = [[NSDateFormatter alloc] init];
    [formatterEnd setDateFormat:@"EEEE"];
    NSString *strDateEnd = [formatterEnd stringFromDate:fechaFromStringEnd];
    
    NSDateFormatter *formatter2End = [[NSDateFormatter alloc] init];
    [formatter2End setDateFormat:@"MMMM"];
    
    
    NSString *pmAMEnd=@"AM";
    if (hourEnd>=12) {
        pmAMEnd=@"PM";
    }

    //
    NSString *fechaEnviarEnd =  [NSString stringWithFormat:@"%@ %ld, %ld.00 %@",strDateEnd,(long)dayEnd,(long)hourEnd,pmAMEnd];

    //

    //Formateos fecha y hora.
    NSString *dateStart= [[arrEventos objectAtIndex:indexPath.row] objectForKey:@"dateStart"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //[df setDateStyle:NSDateFormatterMediumStyle];
    NSDate *fechaFromString = [df dateFromString: dateStart];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitHour fromDate:fechaFromString];
    
    NSInteger day= [components day];
    NSInteger hour =[components hour];
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE"];
    NSString *strDate = [formatter stringFromDate:fechaFromString];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"MMMM"];
    NSString *strMonth = [formatter2 stringFromDate:fechaFromString];

    
    NSString *pmAM=@"AM";
    if (hour>=12) {
        pmAM=@"PM";
    }
    
  
    NSString *horaEnviar= [NSString stringWithFormat:@"%ld.00 %@",(long)hour,pmAM];
    //
    NSString *fechaEnviar =  [NSString stringWithFormat:@"%@ %ld",strDate,(long)day];
    NSString *MesEnviar =  strMonth;

    dictSend =[[NSMutableDictionary alloc] init];
    
    
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"imageUrl"] forKey:@"imgUrl"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"title"] forKey:@"title"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"price"] forKey:@"price"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"city"] forKey:@"city"];
    [dictSend setObject:fechaEnviarEnd forKey:@"dateEnd"];
    [dictSend setObject:fechaEnviar forKey:@"fechaEnviar"];
    [dictSend setObject:MesEnviar forKey:@"MesEnviar"];
    [dictSend setObject:horaEnviar forKey:@"horaEnviar"];
    //[dictSend setObject:dateStartToSend forKey:@"dateStart"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"description"] forKey:@"description"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"address"] forKey:@"address"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"addressLat"] forKey:@"addresslat"];
    [dictSend setObject:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"addressLng"] forKey:@"addresslng"];
    //[dictSend setObject:[arrImgs objectAtIndex:indexpa] forKey:@"dataImg"];
    
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:dictSend forKey:@"dictSend"];
      APLTimeZoneCell *cell = (APLTimeZoneCell *)[tableView cellForRowAtIndexPath:indexPath];

    imgAux = cell.timeImageView.image;

    CompraVC *compraVC =   [self.storyboard instantiateViewControllerWithIdentifier:@"CompraVC"];
    compraVC.image = imgAux;
    [self.navigationController pushViewController:compraVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==2) {
        return 360;
    }
    else
    {
        
        return 195;
    
    }
    
}




- (void)checkButtonTapped:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    UIButton* myButton = (UIButton*)sender;
    
    myButton.transform = CGAffineTransformMakeScale(1.5,1.5);
    myButton.alpha = 0.0f;
    
    [UIView beginAnimations:@"button" context:nil];
    [UIView setAnimationDuration:1];
    myButton.transform = CGAffineTransformMakeScale(1,1);
    myButton.alpha = 1.0f;
    
    [myButton setImage:[UIImage imageNamed:@"eventos-favoritoPress"] forState:UIControlStateNormal];
    [myButton setUserInteractionEnabled:NO];
    [UIView commitAnimations];
    
    
    
    //onclick para cada botton like de cada celda
    if (indexPath == 0)
    {
        
    }
   
        
}

#pragma mark - Call Webservice

-(void)llamadaServicio{

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", nil];
    NSDictionary *params = @ {@"token" :@"1kA0q553Ybwyn3XF8N6Uiz0e3fjj9I2S"};
    
    
    [manager POST:@"http://joinnus-dev-kmppwbyk5q.elasticbeanstalk.com/api/activities-all" parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         dictResponse = responseObject;
         
         
         if ([[responseObject objectForKey:@"ok"]boolValue] == NO) {

 
         }
         else
         {
             arrEventos = [dictResponse objectForKey:@"rows"];
             [_tableView reloadData];

         }
         
         
     }

     
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];



}
-(void)muestraVCInicio
{
    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:vc animated:NO];
    
    //oculta menu con animacion
     [self.mainSlideMenu closeLeftMenu];
    

    
    

}



#pragma mark - Configuring table view cells

- (void)configureCell:(APLTimeZoneCell *)cell forIndexPath:(NSIndexPath *)indexPath {

    
    
    
    //Descarga asyncronica de las imagenes que vienen del servicio
    NSURL *imageURL = [NSURL URLWithString:[[arrEventos objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize)
     {
         // progression tracking code
         
     }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            
                            
                            if (image)
                            {

                                // Set the image.
                                cell.timeImageView.image = image;
                                
                               // [arrImgs addObject:[NSData dataWithData:UIImageJPEGRepresentation(image,1)]];
                                
                                [SVProgressHUD dismiss];
                            }
                        }];
    
    //

    
    cell.lblDescripcion.text = [[arrEventos objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    NSString *strPrecio= [[arrEventos objectAtIndex:indexPath.row] objectForKey:@"price"];
    NSString *precFormateado = [NSString stringWithFormat:@"S./%@.00",strPrecio];
    cell.lblPrecio.text = precFormateado;
    cell.lblLugar.text = [[arrEventos objectAtIndex:indexPath.row] objectForKey:@"city"];
    
    //Formateos fecha y hora.
    NSString *dateStart= [[arrEventos objectAtIndex:indexPath.row] objectForKey:@"dateStart"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //[df setDateStyle:NSDateFormatterMediumStyle];
    NSDate *fechaFromString = [df dateFromString: dateStart];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitHour fromDate:fechaFromString];
    
    NSInteger day= [components day];
    NSInteger hour =[components hour];
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE"];
    NSString *strDate = [formatter stringFromDate:fechaFromString];
    
    NSString *pmAM=@"AM";
    if (hour>=12) {
        pmAM=@"PM";
    }
    
    NSString *fechaAMostrar= [NSString stringWithFormat:@"%@ %ld, %ld.00 %@",strDate,(long)day,(long)hour,pmAM];
    dateStartToSend = fechaAMostrar;
    //

    cell.lblHoraFecha.text = fechaAMostrar;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    //CGRect originFrame = cell.frame;

    cell.backgroundColor = [UIColor clearColor];
    cell.alpha = 0.1f;
    //cell.frame = CGRectMake(originFrame.origin.x + 30, originFrame.origin.y, originFrame.size.width, originFrame.size.height);

    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.alpha = 1.0f;
                         //cell.frame = originFrame;
                     } completion:nil];
}
- (IBAction)onClickBuscarEvento:(UIButton *)sender {
    
    PruebaVC *pruebaVC =   [self.storyboard instantiateViewControllerWithIdentifier:@"pruebaVC"];

    [self.navigationController pushViewController:pruebaVC animated:YES];
}
@end
