//
//  AppDelegate.m
//  JoinUs-iPhone
//
//  Created by bruno on 20/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    // Logs 'install' and 'app activate' App Events.
    [FBAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error

{
    
    // If the session was opened successfully
    
    if (!error && state == FBSessionStateOpen){
        
        NSLog(@"Session opened");
        
        // Show the user the logged-in UI

        return;
        
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        


    }
    
    
    
    // Handle errors
    
    if (error){
        
        NSLog(@"Error");
        
        NSString *alertText;
        
        NSString *alertTitle;
        
        // If the error requires people using an app to make an action outside of the app in order to recover
        
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            
            alertTitle = @"Something went wrong";
            
            alertText = [FBErrorUtility userMessageForError:error];

            
        } else {
            
            
            
            // If the user cancelled login, do nothing
            
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                
                NSLog(@"User cancelled login");
                
                
                
                // Handle session closures that happen outside of the app
                
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){

                
                
                
                // Here we will handle all other errors with a generic error message.
                
                // We recommend you check our Handling Errors guide for more information
                
                // https://developers.facebook.com/docs/ios/errors/
                
            } else {
                

                
            }
            
        }
        
        // Clear this token
        
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // Show the user the logged-out UI

        
    }
    
}



@end
