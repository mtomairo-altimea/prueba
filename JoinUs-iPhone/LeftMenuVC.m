//
//  LeftMenuVC.m
//  AMSlideMenu
//
// The MIT License (MIT)
//
// Created by : arturdev
// Copyright (c) 2014 SocialObjects Software. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

#import "LeftMenuVC.h"
#import "EventosVC.h"
#import "ViewController.h"
#import "UIViewController+AMSlideMenu.h"

#define cellHeight 80 // You can change according to your req.<br>
#define cellWidth 320
@interface LeftMenuVC()

@property (strong, nonatomic) UITableView *myTableView;

@end

@implementation LeftMenuVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    _viewHeader.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:48.0f/255.0f blue:44.0f/255.0f alpha:1.0f];
    
    //Boton de FB
    FBLoginView *loginView = [[FBLoginView alloc] init];
    loginView.frame = CGRectMake(20, 275,150, 50);
    loginView.delegate=self;
    
    for (id obj in loginView.subviews)
    {
        
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel *loginLabel =  obj;
            loginLabel.text = @"   Cerrar Sesión";
            loginLabel.backgroundColor = [UIColor blackColor];
            UIFont *myFont = [ UIFont fontWithName: @"Helvetica" size: 12.0 ];
            loginLabel.font  = myFont;
            loginLabel.frame = CGRectMake(0, 0, 150, 45);
            
            UIImage *img=[UIImage imageNamed:@"menu-cerrarsesionIcon"];
            UIImageView *imgv=[[UIImageView alloc] initWithImage:img];
            [imgv setFrame:CGRectMake(8, 10, imgv.frame.size.width, imgv.frame.size.height)];
            [loginLabel addSubview:imgv];
            
        }
    }
    
    [self.view addSubview:loginView];
    
    indicaSesion = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"fbid"];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(muestraUserData) name:@"muestraUserData" object:nil];

}

-(void)muestraUserData
{
    //Labels con la informacion del usuario
    NSString *fbid = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"fbid"];
    NSString *fbname = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"fbname"];
    
    
    _lblNomUsuario.text = fbname;
    
    //Imagen de perfil
    _profilePictureView=[[FBProfilePictureView alloc] init];
    _profilePictureView.frame=CGRectMake(10, 10, 50, 50);
    
    _profilePictureView.layer.cornerRadius = _profilePictureView.frame.size.width /2;
    _profilePictureView.layer.masksToBounds = YES;
    
    _profilePictureView.profileID = fbid;
    
    [self.view addSubview:_profilePictureView];
    
    NSLog(@"Conectadooooo");

}

-(void)viewDidAppear:(BOOL)animated
{
    


    
    
}



#pragma mark FBDelegados
/*
//
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    
    LoguinViewController *vc= [[LoguinViewController alloc] init];
    
    [self.navigationController pushViewController:vc animated:NO];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    
    //resetear variables NSUSerDefault
    NSDictionary *defaultsDictionary = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    for (NSString *key in [defaultsDictionary allKeys]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}*/
-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
    _lblNomUsuario.text=@"No has iniciado sesion";
    _profilePictureView.profileID = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"muestraVCInicio" object:nil];

    //desactivar Gestos para menu
    [self disableSlidePanGestureForLeftMenu];

}

-(void)mostrarViewInicio{


}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    
    EventosVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"eventosVC"];
    [self.navigationController pushViewController:vc animated:YES];
    
}




@end
