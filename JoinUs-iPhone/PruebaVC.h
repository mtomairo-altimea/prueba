//
//  PruebaVC.h
//  JoinUs-iPhone
//
//  Created by Miguel Tomairo on 10/02/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PruebaVC : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *buscarCategButton;


- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickBuscar:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *buscarTextField;

@end
