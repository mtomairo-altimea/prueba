//
//  PruebaVC.m
//  JoinUs-iPhone
//
//  Created by Miguel Tomairo on 10/02/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import "PruebaVC.h"
#import "TableCell.h"

@interface PruebaVC ()

@property (weak, nonatomic) IBOutlet UITableView *tbl;

@end

@implementation PruebaVC
{
    NSArray *icono;
    NSArray *nombre;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _buscarTextField.alpha = 0.0;
    [_buscarTextField setDelegate:self];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    [gestureRecognizer setCancelsTouchesInView:NO];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    _buscarTextField.leftView = paddingView;
    _buscarTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    icono = [NSArray arrayWithObjects:@"categorias-arte@2x.png",@"categorias-ayuda@2x.png",@"categorias-charlas@2x.png",@"categorias-comidas@2x.png",@"categorias-deportes@2x.png",@"categorias-entreteni@2x.png",@"categorias-hobbies@2x.png",@"categorias-mascotas@2x.png", nil];
    
    nombre = [NSArray arrayWithObjects:@"ARTE Y CULTURA",@"AYUDA SOCIAL",@"CHARLAS Y CONFERENCIAS",@"COMIDAS Y BEBIDAS",@"DEPORTES",@"ENTRETENIMIENTO",@"HOBBIES Y MANUALIDADES",@"MASCOTAS Y ANIMALES", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [icono count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Celda";
    
        
        TableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // Configure the cell...
        int row = (int)[indexPath row];
        
        cell.imgCategoria.contentMode = UIViewContentModeCenter;
        cell.imgCategoria.image = [UIImage imageNamed:icono[row]];
        
        
        UIFont *myFont = [UIFont fontWithName: @"Arial" size: 11.0];
        cell.lblTituloCateg.font = myFont;
        cell.lblTituloCateg.text = nombre[row];
        
        cell.imgDiclosure.contentMode = UIViewContentModeCenter;
        cell.imgDiclosure.image = [UIImage imageNamed:@"categorias-iconCelda@2x.png"];
        
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self.navigationController showViewController:vc sender:self];
    //[self.navigationController showDetailViewController:vc sender:self];
    
    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"eventosVC"];

    /*
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    
    [self.navigationController pushViewController:vc animated:NO];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
    
    [UIView commitAnimations];*/
    
    /* 
     
    // Codigo Bruno
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    
    UIView *containerView = self.view.window;
    [containerView.layer addAnimation:transition forKey:nil];
     
     */
    
    // Codigo Rapser
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    UIView *containerView = self.view.window;
    [containerView.layer addAnimation:animation forKey:nil];
    
    [self.navigationController pushViewController:vc animated:NO];
    
}

#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}
*/

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    if ([touch.view isKindOfClass:[UIBarButtonItem class]]) {
        return NO;
    }
    return YES;
}


- (void)hideKeyboard
{
    
    [_buscarTextField resignFirstResponder];

}


- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickBuscar:(UIButton *)sender {
    
    _buscarTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    CGRect newFrame = CGRectMake(_buscarTextField.frame.origin.x, _buscarTextField.frame.origin.y - 38,_buscarTextField.frame.size.width, _buscarTextField.frame.size.height);
    
    [UIView animateWithDuration:0.3f
                          delay:0.3f
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         [_buscarTextField setFrame:newFrame ], _buscarTextField.alpha = 0.7,_buscarCategButton.alpha = 0.0;
                         
                     }
                     completion:nil];
    
}

-(void) prueba{
    
}

#pragma mark - UITextFieldDelegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [_buscarTextField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField{
    
    _buscarTextField.text = @"";
    [_buscarTextField resignFirstResponder];
    return NO;
}

@end
