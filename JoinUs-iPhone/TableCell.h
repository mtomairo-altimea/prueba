//
//  TableCell.h
//  JoinUs-iPhone
//
//  Created by Miguel Tomairo on 3/02/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCategoria;
@property (weak, nonatomic) IBOutlet UILabel *lblTituloCateg;
@property (weak, nonatomic) IBOutlet UIImageView *imgDiclosure;

@end
