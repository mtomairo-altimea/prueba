//
//  ViewController.h
//  JoinUs-iPhone
//
//  Created by bruno on 20/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface ViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate,FBLoginViewDelegate>
{
    
    BOOL textoAnimado;
    BOOL editando;
    BOOL primeraVez;
    NSData *recivedData;
    NSMutableDictionary *usuArr;
    NSDictionary *jsonDict;

    FBLoginView *loginView;
    
    FBProfilePictureView *profilePictureView;

}



@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewAux;

@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
- (IBAction)onClickIngresar:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnFB;

@property (weak, nonatomic) IBOutlet UIImageView *imgvSeparator;
@property (weak, nonatomic) IBOutlet UITextField *txtContrasena;
- (IBAction)onClickIngresaContuCorreo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnIngresacontucorreo;
@property (weak, nonatomic) IBOutlet UIButton *btnRegistrate;
@property (weak, nonatomic) IBOutlet UIButton *btnNoUsuarioAun;

@end

