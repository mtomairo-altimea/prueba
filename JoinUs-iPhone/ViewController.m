//
//  ViewController.m
//  JoinUs-iPhone
//
//  Created by bruno on 20/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import "ViewController.h"
#import "UIViewController+AMSlideMenu.h"
#import "UIColor+CreateMethods.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "EventosVC.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView_loguin;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //desactivar Gestos para menu
    [self disableSlidePanGestureForLeftMenu];
   // [self addLeftMenuButton];
    
    //FB Button
    loginView =
    [[FBLoginView alloc] initWithReadPermissions:
     @[@"public_profile", @"email", @"user_friends"]];
    [loginView setFrame:CGRectMake(21, 205, 277, 50)];
    loginView.delegate = self;
    for (id obj in loginView.subviews)
    {
        
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.text = @"Iniciar sesión con Facebook";
            
            loginLabel.frame = CGRectMake(24, 0, 250, 43);
        }
    }
    
    
    [self.view addSubview:loginView];

    //Recognizer
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate=self;
    
    [self.view addGestureRecognizer:tapGestureRecognizer];
    textoAnimado = NO;
    editando = NO;
    primeraVez = YES;
    
    //Animacion truco
    [UIView animateWithDuration:0.01
                          delay:0.01
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         [_scrollView_loguin setFrame:CGRectMake (13,
                                                                  275,
                                                                  294,
                                                                  129)];
   
                     }
                     completion:^(BOOL finished){
                         
                         
                         [UIView animateWithDuration:0.01
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              
                                              [_scrollView_loguin setFrame:CGRectMake (13,
                                                                                       275,
                                                                                       294,
                                                                                       129)];
                                              primeraVez = YES;
                                              
                                          }
                                          completion:nil];
                     }];
    
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         loginView.alpha = 1;
                         _imgvSeparator.alpha = 1;
                         
                         
                         
                     }
                     completion:nil];
    _txtContrasena.text = @"";
    _txtUsuario.text = @"";
    [_txtUsuario resignFirstResponder];
    [_txtContrasena resignFirstResponder];
    
    
//




    [_txtUsuario addTarget:self
                  action:@selector(textUsuarioDidChange)
        forControlEvents:UIControlEventEditingChanged];
    [_txtContrasena addTarget:self
                    action:@selector(textContrasenaDidChange)
          forControlEvents:UIControlEventEditingChanged];



}
-(void)viewDidAppear:(BOOL)animated
{
    //desactivar Gestos para menu
    [self disableSlidePanGestureForLeftMenu];

}

- (void)textUsuarioDidChange
{
    primeraVez = NO;
    
    
    [UIView animateWithDuration:0.001
                          delay:0.001
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [_scrollView_loguin setFrame:CGRectMake (13,
                                                                  175,
                                                                  294,
                                                                  129)];
                         editando = YES;
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              
                                              [_scrollView_loguin setFrame:CGRectMake (13,
                                                                                       175,
                                                                                       294,
                                                                                       129)];
                                              
                                              
                                              
                                          }
                                          completion:^(BOOL finished){
                                              
                                              
                                              
                                          }];
                     }];
    
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         loginView.alpha = 0;
                         _imgvSeparator.alpha = 0;
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         
                         
                         
                     }];
   
}

- (void)textContrasenaDidChange
{
    primeraVez = NO;
   [UIView animateWithDuration:0.001
                          delay:0.001
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [_scrollView_loguin setFrame:CGRectMake (13,
                                                                  175,
                                                                  294,
                                                                  129)];
                         editando = YES;
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              
                                              [_scrollView_loguin setFrame:CGRectMake (13,
                                                                                       175,
                                                                                       294,
                                                                                       129)];

                                          }
                                          completion:^(BOOL finished){

                                          }];
                     }];
    
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         loginView.alpha = 0;
                         _imgvSeparator.alpha = 0;
                         
                     }
                     completion:^(BOOL finished){

                     }];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    NSLog(@"click");
    if (primeraVez) {
        
    editando = YES;
        
    _scrollView_loguin.alpha = 0;
    [_scrollViewAux setHidden:YES];
        
    [UIView animateWithDuration:0.001
                          delay:0.01
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [_scrollView_loguin setFrame:CGRectMake (13,
                                                                  175,
                                                                  294,
                                                                  129)];
                         
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              _scrollView_loguin.alpha = 1;
                                              [_scrollView_loguin setFrame:CGRectMake (13,
                                                                                       175,
                                                                                       294,
                                                                                       129)];
                                              
                                              
                                          }
                                          completion:nil];
                     }];
    
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         loginView.alpha = 0;
                         _imgvSeparator.alpha = 0;
                         
                         
                         
                     }
                     completion:nil];
    

}
    
}


- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
   
    if (editando) {
        
    
        [UIView animateWithDuration:0.001
                              delay:0.01
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             
                             [_scrollView_loguin setFrame:CGRectMake (13,
                                                                      275,
                                                                      294,
                                                                      129)];
                             
                             editando = NO;
                             
                             
                             
                             
                         }
                         completion:^(BOOL finished){
                             
                             
                             [UIView animateWithDuration:0.5
                                                   delay:0
                                                 options:UIViewAnimationOptionBeginFromCurrentState
                                              animations:^{
                                                  
                                                  [_scrollView_loguin setFrame:CGRectMake (13,
                                                                                           275,
                                                                                           294,
                                                                                           129)];
                                                  primeraVez = YES;
                                                  
                                              }
                                              completion:nil];
                         }];
        
        
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             loginView.alpha = 1;
                             _imgvSeparator.alpha = 1;
                             
                            
                             
                         }
                         completion:nil];
    _txtContrasena.text = @"";
    _txtUsuario.text = @"";
    [_txtUsuario resignFirstResponder];
    [_txtContrasena resignFirstResponder];
        
    

    }
    
}


- (IBAction)onClickIngresar:(id)sender {

    
    [SVProgressHUD show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", nil];
    
    NSString *usu=_txtUsuario.text;
    NSString *pass=_txtContrasena.text;
    
    NSDictionary *parameters = @{@"email": usu,
                                 @"password": pass};
    

    [manager POST:@"http://joinnus-dev-kmppwbyk5q.elasticbeanstalk.com/api/login" parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);

         if ([[responseObject objectForKey:@"ok"]boolValue] != NO) {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Joinnus"
                                                             message:@"Email/Password erroneos"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
             [alert show];
             [SVProgressHUD dismiss];

         }
         else
         {
             [_txtContrasena resignFirstResponder];
             [_txtUsuario resignFirstResponder];
             
             [SVProgressHUD dismiss];

             EventosVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"eventosVC"];
             [self.navigationController pushViewController:vc animated:YES];
         }
         
         
     }
     
     
     
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];
    
    /*NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://joinnus-dev-kmppwbyk5q.elasticbeanstalk.com/api/login"]];
    
    [request setHTTPMethod:@"POST"];
    [request addValue:@"postValues" forHTTPHeaderField:@"METHOD"];
    
    //create data that will be sent in the post
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:_txtUsuario.text forKey:@"email"];
    [dictionary setValue:_txtContrasena.text forKey:@"password"];
    
    //serialize the dictionary data as json
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:data]; //set the data as the post body
    //[request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)data.length] forHTTPHeaderField:@"Content-Length"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }*/
  
}







// connection NSURLDELgate
#pragma mark - Data connection delegate -

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{ // executed when the connection receives data
    
    recivedData = data;
    /* NOTE: if you are working with large data , it may be better to set recievedData as NSMutableData
     and use  [receivedData append:Data] here, in this event you should also set recievedData to nil
     when you are done working with it or any new data received could end up just appending to the
     last message received*/
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{ //executed when the connection fails
    
    NSLog(@"Connection failed with error: %@",error.localizedDescription);
}
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    
    /*This message is sent when there is an authentication challenge ,our server does not have this requirement so we do not need to handle that here*/
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    NSLog(@"Request Complete,recieved %lu bytes of data",(unsigned long)recivedData.length);
    
    //[self.delegate requestReturnedData:receivedData];//send the data to the delegate
}


#pragma mark FB Methods


// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    
    
    //LLenar Data de usuario en Userdefaults y mandarlo por NotificationCenter
    
    NSLog(@"%@",[user objectForKey:@"name"]);
    NSLog(@"%@",[user objectForKey:@"gender"]);
    NSLog(@"%@",[user objectForKey:@"id"]);
    NSLog(@"%@",[user objectForKey:@"link"]);
    NSLog(@"%@",[user objectForKey:@"username"]);
    
    
    
    NSLog(@"%@",[[user objectForKey:@"hometown"] objectForKey:@"name"]);
    
    
    //Guardar datos en el NSUserDefault
    

    NSString *ide = user.id;
    NSString *nom= [user objectForKey:@"name"];


    [ [NSUserDefaults standardUserDefaults] setObject:ide forKey:@"fbid"];
    [ [NSUserDefaults standardUserDefaults] setObject:nom forKey:@"fbname"];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"muestraUserData" object:nil];

   
}


- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    
    //
    //  [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[InfoUsuarioViewController alloc] init]]
    //                                              animated:NO];
    
    // [self.navigationController setNavigationBarHidden:YES];
    
    
    EventosVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"eventosVC"];
    [self.navigationController pushViewController:vc animated:YES];
}



// This method will handle ALL the session state changes in the app
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        // Show the user the logged-in UI
        
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
       
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            //[self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
               // [self showMessage:alertText withTitle:alertTitle];
                
                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                //[self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
       // [self userLoggedOut];
    }
}

        

- (IBAction)onClickIngresaContuCorreo:(id)sender {
    
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [_scrollViewAux setFrame:CGRectMake(_scrollViewAux.bounds.origin.x,410, _scrollViewAux.bounds.size.width, _scrollViewAux.bounds.size.height)];
                         [_btnIngresacontucorreo setHidden:YES];
                         
                     }
                     completion:nil];


}



@end
